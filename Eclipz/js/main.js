$(document).ready(function() {
    let btnNav = $('.hb-icon-btn');
    btnNav.click(
        function() {
            let navUl = $('.navbar-ul');
            let right = $('.hb-icon1');
            let center = $('.hb-icon2');
            let left = $('.hb-icon3');
            if(!btnNav.hasClass('nav-is-open')) {
                btnNav.addClass('nav-is-open');
                navUl.addClass('nav-open');
                right.addClass('hb-right');
                center.addClass('hb-center');
                left.addClass('hb-left');
            } else {
                btnNav.removeClass('nav-is-open');
                navUl.removeClass('nav-open');
                right.removeClass('hb-right');
                center.removeClass('hb-center');
                left.removeClass('hb-left');
            }
        }
    )
});