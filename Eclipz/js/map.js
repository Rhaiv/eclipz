function initMap() {
    let eclipz = {lat: 35.813307, lng: -90.712964};
    let contentString = "<div class='eclipz-address' style='color: #000;z-index:1;'>" +
        "<address style='font-style:unset;'>" +
        "<strong>Eclipz Salon & Spa</strong><br />" +
        "2704 South Culberhouse Street<br />" +
        "Jonesboro, AR  72404" +
        "</address>" +
        "<a target='_blank' href='https://www.google.com/maps/dir//Eclipz+Salon+%26+Spa,+2704+S+Culberhouse+St,+Jonesboro,+AR+72401,+USA/@35.813257,-90.7135032,19z/data=!4m8!4m7!1m0!1m5!1m1!1s0x87d42ed84bafa7f3:0xce385eaab441d5fd!2m2!1d-90.712956!2d35.813257' class='directions' style='color:#0000ff;text-decoration:underline;'>Get Directions</a>" +
        "</div>";
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 35.813405, lng: -90.712999},
        zoom: 15,
        mapTypeControl: false,
        fullscreenControl: false,
        streetViewControl: false
    });

    let marker = new google.maps.Marker({
        position: eclipz,
        map: map,
        title: "Eclipz Salon & Spa"
    });

    let tooltip = new google.maps.InfoWindow({
        content: contentString
    });

    marker.addListener('click', function () {
        tooltip.open(map, marker);
    });
}